/*
 * This file is part of the Eastmore Management, LLC.
 *
 * Copyright (c) 1988-1993 The Regents of the University of California.
 * Copyright (c) 1994 Sun Microsystems, Inc.
 * Copyright (c) 2019, by the Eastmore Management, LLC.
 * This document and all information contained herein
 * (a) constitute confidential proprietary information and intellectual property
 * of Eastmore Management, LLC and its affiliates ("Eastmore").
 * (b) shall be kept confidential and may not be disclosed by the recipient in
 * whole or in part to any other person (except for other Eastmore employees
 * who need to know such information and are bound by similar confidentiality
 * restrictions).
 * (c) may not be used or exploited by the recipient or any other person in
 * whole or in part except for the purpose for which this document has been
 * disclosed to the recipient in connection with the recipient's provision of
 * assigned services to Eastmore.
 * (d) may not be copied, transferred, transmitted, distributed or reproduced
 * in any manner or any media without Eastmore's prior written consent.
 */


